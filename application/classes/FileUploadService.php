<?php

class FileUploadService extends Extendable {
    /**
     * @var FileUploadService
     */
    public static $singleton = null;

    public static function changeSingleton(FileUploadService $s) {
        self::$singleton = $s;
    }

    public function __construct($properties = array()) {
        parent::__construct($properties);
    }

    public function upload($pathGenerationClosure) {

    }
}

if(FileUploadService::$singleton === null) {
    FileUploadService::changeSingleton(new FileUploadService());
}