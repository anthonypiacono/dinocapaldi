<?php

class ReflectionService extends Extendable {

    /**
     * @var ReflectionService
     */
    public static $singleton = null;

    public static function changeSingleton(ReflectionService $s) {
        self::$singleton = $s;
    }

    //

    private $reflection_cache = array();

    private function ParseAttributes(ReflectionMethod $method) {
        $func_doc = $method->getDocComment();

        preg_match_all('/\@(.*+)/', $func_doc, $matches);

        return empty($matches) ? array() : \Plinq\Plinq::factory(array_values(array_unique($matches[1])))->Select(function($_, $v) {
            return trim($v);
        })->ToArray();
    }

    public function getClassFunctionAttributes($classObject) {
        $className = get_class($classObject);

        $classMethods = empty($this->reflection_cache[$className]) ? null : $this->reflection_cache[$className];

        if ($classMethods === null) {
            $tempMethods = get_class_methods($classObject);
            $classMethods = array();

            foreach ($tempMethods as $method_name) {
                $method = new ReflectionMethod("{$className}::{$method_name}");
                $classMethods[$method_name] = $this->ParseAttributes($method);
            }

            $this->reflection_cache[$className] = $classMethods;
        }

        return $classMethods;
    }
}

if(ReflectionService::$singleton === null) {
    ReflectionService::changeSingleton(new ReflectionService());
}
