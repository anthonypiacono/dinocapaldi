<?php

class EmailService extends Extendable {
    /**
     * @var EmailService
     */
    public static $singleton = null;

    public static function changeSingleton(EmailService $s) {
        self::$singleton = $s;
    }

    /**
     * @var Swift_SmtpTransport
     */
    public $transport;

    /**
     * @var Swift_Mailer
     */
    public $mailer;

    public $database;

    public function __construct($properties = array()) {
            parent::__construct($properties);

        $this->transport = Swift_SmtpTransport::newInstance(Config::$Configs['email']['smtp']['host'], Config::$Configs['email']['smtp']['port']);

        if(Config::$Configs['email']['smtp']['user'] !== null) {
            $this->transport->setUsername(Config::$Configs['email']['smtp']['user'])
                ->setPassword(Config::$Configs['email']['smtp']['pass']);
        }

        $this->mailer = Swift_Mailer::newInstance($this->transport);

        $this->database = MySQLPool::$singleton->database(Config::$Configs['mysql']);
    }

    public $actorStack = array();
    
    public function push_actor(UserModel $actor) {
        $this->actorStack[] = $actor;
    }

    public function pop_actor() {
        $this->actorStack = array_slice($this->actorStack, 0, count($this->actorStack) - 1);
    }

    /**
     * @return null|UserModel
     */
    public function get_current_actor() {
        $actorCount = count($this->actorStack);

        if($actorCount == 0) {
            return null;
        }

        return $this->actorStack[$actorCount - 1];
    }

    public $projectStack = array();

    public function push_project(ProjectModel $project) {
        $this->projectStack[] = $project;
    }

    public function pop_project() {
        $this->projectStack = array_slice($this->projectStack, 0, count($this->projectStack) - 1);
    }

    /**
     * @return null|ProjectModel
     */
    public function get_current_project() {
        $projectCount = count($this->projectStack);

        if($projectCount == 0) {
            return null;
        }

        return $this->projectStack[$projectCount - 1];
    }

    public function sendEmail($subject, $body, $to = array(), $bcc = array(), $cc = array(), $from = null, $from_name = null, $reply_to = null, $reply_to_name = null, $send_now = false) {
        $from = $from === null ? Config::$Configs['email']['from'] : $from;
        $from_name = $from_name === null ? Config::$Configs['email']['from_name'] : $from_name;

        $reply_to = $reply_to === null ? $from : $reply_to;
        $reply_to_name = $reply_to_name === null ? $from_name : $reply_to_name;

        $to = is_array($to) ? $to : array($to);
        $bcc = is_array($bcc) ? $bcc : array($bcc);
        $cc = is_array($cc) ? $cc : array($cc);

        $message = Swift_Message::newInstance($subject, $body, 'text/html', 'utf-8')
            ->setCc($cc)->setBcc($bcc)->setReplyTo($reply_to, $reply_to_name)->setFrom($from, $from_name)->setTo($to);

        if(!$send_now) {
            $dateTimeEncoded = $this->database->encode(StrLib::DateTime());
            $dataEncoded = $this->database->encode(serialize($message));

            $this->database->query("INSERT INTO `sas_email_queue` (`datetime`, `data`) VALUES ({$dateTimeEncoded}, {$dataEncoded})");

            return 0;
        }

        return $this->mailer->send($message);
    }

    public function sendSmartEmail($email_key, $email_address, $variables) {
        if(!Config::$Configs['email']['enabled']) {
            return 1;
        }

        $layout = array('email');

        // Swap in a new baseUri
        $oldBaseUri = UriBusinessRules::$singleton->baseUri;
        UriBusinessRules::$singleton->change_base_uri(Config::$Configs['application']['paths']['base_url']);

        $body = ViewRenderingService::$singleton->renderView('emails/' . $email_key . 'Email', $variables, $layout);

        // Replace inline classes
        $body = $this->inlineClasses($body);

        $body = str_replace('http://localhost/', Config::$Configs['email']['body_host'], $body);
        
        $subject = ViewRenderingService::$singleton->renderView('email_subjects/' . $email_key . 'EmailSubject', $variables);

        // Put the old one back
        UriBusinessRules::$singleton->change_base_uri($oldBaseUri);

        $to = array($email_address);

        $fromAddress = Config::$Configs['email']['from'];
        $fromName = $this->get_from_name();

        $replyToAddress = $fromAddress;

        $sendNow = Config::$Configs['email']['delayed'] ? false : true;

        return $this->sendEmail($subject, $body, $to, array(), array(), $fromAddress, $fromName, $replyToAddress, null, $sendNow);
    }

    public function sendUserEmail($email_key, UserModel $user_model, $variables, $groupModel = null, $rootKey = null) {
        if(!Config::$Configs['email']['enabled']) {
            return 1;
        }

        if(false === $user_model->getEmailPreference($email_key)) {
            return null;
        }

        // Don't send the email to the actor
        $currentActor = $this->get_current_actor();

        if($currentActor !== null && $currentActor->id == $user_model->id && empty($user_model->email_receipts)) {
            return null;
        }

        $layout = array('email');

        $variables = (array)$variables;
        $variables['emailUserModel'] = $user_model;
        $variables['emailGroupModel'] = $groupModel;

        LanguageLocalizer::$singleton->push_language($user_model->language);
        TimezoneLocalizer::$singleton->push_timezone($user_model->timezone);

        // Swap in a new baseUri
        $oldBaseUri = UriBusinessRules::$singleton->baseUri;
        UriBusinessRules::$singleton->change_base_uri(Config::$Configs['application']['paths']['base_url']);

        $body = ViewRenderingService::$singleton->renderView('emails/' . $email_key . 'Email', $variables, $layout);

        // Replace inline classes
        $body = $this->inlineClasses($body);

        $body = str_replace('http://localhost/', Config::$Configs['email']['body_host'], $body);

        $subject = ViewRenderingService::$singleton->renderView('email_subjects/' . $email_key . 'EmailSubject', $variables);

        // Put the old one back
        UriBusinessRules::$singleton->change_base_uri($oldBaseUri);

        LanguageLocalizer::$singleton->pop_language();
        TimezoneLocalizer::$singleton->pop_timezone();

        $emailAddress = !empty($user_model->email_address) ? $user_model->email_address : $user_model->email_verify;

        $to = array(
            $emailAddress => $user_model->getDisplayName($user_model->language)
        );

        $fromAddress = Config::$Configs['email']['from'];
        $fromName = $this->get_from_name();
        
        $replyToAddress = $fromAddress;

        if($rootKey !== null) {
            $ourKey = StrLib::Guidish();
            $this->registerEmailKey($user_model, $rootKey, $ourKey);

            $replyToAddress = sprintf(Config::$Configs['email']['from_key'], $ourKey);
        }

        $sendNow = Config::$Configs['email']['delayed'] ? false : true;

        return $this->sendEmail($subject, $body, $to, array(), array(), $fromAddress, $fromName, $replyToAddress, null, $sendNow);
    }

    public function sendGroupEmails($email_key, GroupModel $group_model, $variables, $rootKey = null) {
        // Get all the users in the group.
        $userGroupModels = ViewQueryFactory::$singleton->getUserGroupModelsByGroupId($group_model->id);

        foreach($userGroupModels as $userGroupModel) {
            $this->sendUserEmail($email_key, $userGroupModel->user_model, $variables, $group_model, $rootKey);
        }
    }

    /**
     * @param UserModel $userModel
     * @param $rootKey
     * @param $ourKey
     */
    public function registerEmailKey(UserModel $userModel, $rootKey, $ourKey) {
        ModelCreationService::$singleton->createEmailKey(new NewEmailKeyModel(array(
            'user_id' => $userModel->id,
            'root_key' => $rootKey,
            'our_key' => $ourKey
        )));
    }

    private function inlineClasses($body) {
        $themeConfig = ThemeService::$singleton->getThemeConfigModel();

        $body = preg_replace_callback("/class\\=\\\"([a-zA-Z0-9\\ \\_\\-]+)\\\"/", function($matches) use($themeConfig) {
            $classes = $matches[1];

            $classNames = explode(' ', $classes);

            $styleRules = '';

            foreach($classNames as $className) {
                if(empty(EmailBusinessRules::$singleton->inline_css_rules[$className])) {
                    continue;
                }

                $classRules = preg_replace('/\\;$/', '', trim(EmailBusinessRules::$singleton->inline_css_rules[$className])) . ';';

                $styleRules .= $classRules;
            }

            if(empty($styleRules)) {
                return "";
            }

            if($themeConfig->version != 1) {
                $styleRules = preg_replace_callback('/([a-z_\\-]+)\\.png/', function($matches) use($themeConfig) {
                    if(!in_array($matches[1], EmailBusinessRules::$singleton->blue_email_images)) {
                        return $matches[0];
                    }

                    return $matches[1] . '-' . $themeConfig->version . '.png';
                }, $styleRules);
            }

            return "style=\"{$styleRules}\"";
        }, $body);

        return $body;
    }

    public function get_from_name() {
        $fromName = lang_get('email_default_from_name');

        $currentActor = $this->get_current_actor();
        $currentProject = $this->get_current_project();

        // If there is a project OR an actor, make a better fromName
        if($currentActor !== null || $currentProject !== null) {
            if($currentActor === null && $currentProject !== null) {
                $fromName = sprintf(lang_get('email_project_from_name_format'), $currentProject->getDisplayName());
            }
            else if($currentActor !== null && $currentProject === null) {
                $fromName = sprintf(lang_get('email_user_from_name_format'), $currentActor->getDisplayName());
            }
            else {
                $fromName = sprintf(lang_get('email_project_and_user_from_name_format'), $currentProject->getDisplayName(), $currentActor->getDisplayName());
            }
        }

        return $fromName;
    }
}

if(EmailService::$singleton === null) {
    EmailService::changeSingleton(new EmailService());
}