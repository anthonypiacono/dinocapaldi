<?php

class UriBusinessRules extends Extendable {
    /**
     * @var UriBusinessRules $singleton
     */
    public static $singleton = null;
    
    public static function changeSingleton(UriBusinessRules $s) {
        self::$singleton = $s;
    }

    public $baseUri = '';

    public function change_base_uri($newBaseUri = '') {
        $this->baseUri = $newBaseUri;
    }

    public function format_uri($relative_uri) {
        $relative_uri = preg_replace('/^\\//', '', $relative_uri);
        return preg_replace('/\\/$/', '', $this->baseUri) . '/sas/v2/' . $relative_uri;
    }

    public function getGlobalDashboardUri() {
        return $this->format_uri('');
    }

    public function getGlobalCalendarUri($monthYear = null) {
        $format = 'calendar';

        if($monthYear !== null) {
            $monthYear = (object)$monthYear;
            $format .= '/' . $monthYear->month . '/' . $monthYear->year;
        }

        return $this->format_uri($format);
    }

    public function getUserRuleTransitionUri($key) {
        return $this->format_uri('user-rule/' . $key . '/transition');
    }

    public function getLoginUri($return_uri = null) {
        $format = 'login';

        if($return_uri !== null) {
            $format .= '?return=' . urlencode($return_uri);
        }

        return $this->format_uri($format);
    }

    public function getManageAccountPreferencesUri() {
        return $this->format_uri('account/preferences');
    }

    public function getManageAccountUri() {
        return $this->format_uri('account');
    }

    public function getManageUsersUri() {
        return $this->format_uri('users/manage');
    }

    public function getAccountSetupUri($return_uri = null) {
        $format = 'account/setup';

        if($return_uri !== null) {
            $format .= '?return=' . urlencode($return_uri);
        }

        return $this->format_uri($format);
    }

    public function getManageGroupsUri() {
        return $this->format_uri('groups/manage');
    }

    public function getManageProjectsUri() {
        return $this->format_uri('projects/manage');
    }

    public function getManageThemeUri() {
        return $this->format_uri('theme');
    }

    public function getUpdateThemeUri() {
        return $this->format_uri('update-theme');
    }

    public function getUpdateEmailAddressUri() {
        return $this->format_uri('account/update-email');
    }

    public function getResendVerificationUri() {
        return $this->format_uri('account/resend-verification');
    }

    public function getUpdatePasswordUri() {
        return $this->format_uri('account/update-password');
    }

    public function getNewGroupUri() {
        return $this->format_uri('group/new');
    }

    public function getCreateProjectUri() {
        return $this->format_uri('project/create');
    }

    public function getCreateUserUri() {
        return $this->format_uri('user/create');
    }

    public function getLogoutUri() {
        return $this->format_uri('logout');
    }

    public function getAdminPanelUri() {
        return $this->format_uri('admin');
    }

    public function getFAQUri() {
        return $this->format_uri('faqs');
    }

    public function getContactUri() {
        return $this->format_uri('contact');
    }

    public function getLegalUri() {
        return $this->format_uri('legal');
    }

    public function getForgotPasswordUri() {
        return $this->format_uri('forgot-password');
    }

    public function getGlobalSearchUri() {
        return $this->format_uri('search');
    }

    public function getGlobalDashboardTourUri() {
        return $this->format_uri('tour-global-dashboard');
    }

    public function getGlobalCalendarTourUri() {
        return $this->format_uri('tour-global-calendar');
    }
}

if(UriBusinessRules::$singleton === null) {
    UriBusinessRules::changeSingleton(new UriBusinessRules());
}