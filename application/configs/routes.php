<?php
$config = array(
    'routes' => array(
        new Route(array(
        'pattern' => '/',
        'resource' => 'HomeResource'
        )),
        new Route(array(
            'pattern' => '/post/:post_id',
            'resource' => 'PostResource'
        ))
    )
);