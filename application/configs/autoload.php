<?php

Config::Import('mysql');
Config::Import('application');
Config::Import('email');
Config::Import('formats');

Lib::Import('classes/swiftmailer/lib/swift_required');

Lib::Import('classes/ColorService');
Lib::Import(array(
    'mysql/mysql_pool', 'mysql/mysql_query_benchmark', 'mysql/mysql_fluent_select_query',
    'models/AppModel', 'models/LocalizationModel', 'classes/Plinq', 'validators/simple'
));

spl_autoload_register(function ($classname) {
    if(preg_match('/Model$/', $classname)) {
        Lib::Import('models/' . $classname);
        return;
    }

    if(preg_match('/Command$/', $classname)) {
        Lib::Import('commands/' . $classname);
        return;
    }

    if(preg_match('/Event$/', $classname)) {
        Lib::Import('events/' . $classname);
        return;
    }

    if(preg_match('/CommandHandler$/', $classname)) {
        Lib::Import('commandhandlers/' . $classname);
        return;
    }

    if(preg_match('/CommandResponse$/', $classname)) {
        Lib::Import('commandresponses/' . $classname);
        return;
    }

    if(preg_match('/EventHandler$/', $classname)) {
        Lib::Import('eventhandlers/' . $classname);
        return;
    }

    if(preg_match('/EventResponse$/', $classname)) {
        Lib::Import('eventresponses/' . $classname);
        return;
    }

    if(preg_match('/Resource$/', $classname)) {
        Lib::Import('resources/' . $classname);
        return;
    }

    if(preg_match('/Response$/', $classname)) {
        Lib::Import('responses/' . $classname);
        return;
    }

    if(preg_match('/Validator$/', $classname)) {
        Lib::Import('validators/' . $classname);
        return;
    }

    if(preg_match('/Shell$/', $classname)) {
        Lib::Import('shells/' . $classname);
        return;
    }

    if(preg_match('/Service$/', $classname) || preg_match('/BusinessRules$/', $classname) || in_array($classname, array('RougeModelBinder', 'LocalizationTool', 'ViewQueryFactory', 'LanguageLocalizer', 'TimezoneLocalizer'))) {
        Lib::Import('classes/' . $classname);
        return;
    }
});

if(php_sapi_name() != 'cli') {
    set_error_handler(function($errno , $errstr, $errfile, $errline) {
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

        if($errno == 2048) {
            return;
        }

        echo ViewRenderingService::$singleton->renderView('ErrorBox', array(
            'backtrace' => $backtrace,
            'error' => $errstr,
            'file' => $errfile,
            'line' => $errline,
            'errno' => $errno
        ));
    }, E_ALL);

    set_exception_handler(function(Exception $e) {
        $backtrace = $e->getTrace();

        die(ViewRenderingService::$singleton->renderView('ErrorBox', array(
            'backtrace' => $backtrace,
            'error' => $e->getMessage(),
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'errno' => $e->getCode()
        )));
    });
}

function in_array_model($needle_model, $haystack, $key = 'id') {
    $needle_key = $needle_model->{$key};

    foreach($haystack as $model) {
        if(empty($model->{$key})) {
            continue;
        }

        if($model->{$key} != $needle_key) {
            continue;
        }

        return true;
    }

    return false;
}