<?php

class HomeResource extends AppResource {
    public function execute() {
        return new AppViewResponse($this, array(
            'view' => 'HomePage'
        ));
    }
}