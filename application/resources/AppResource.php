<?php

Lib::Import('mysql/mysql_pool');

Config::Import('mysql');

class AppResource extends Resource {
    /**
     * @var MySQLDatabase
     */
    public $database;

    public function __construct($properties = array()) {
        parent::__construct($properties);

        $this->init_vars();

        $this->database = MySQLPool::$singleton->database(Config::$Configs['mysql']);

        $userModel = AuthenticationService::$singleton->user;
    }

    public function init_vars() {
    }
}