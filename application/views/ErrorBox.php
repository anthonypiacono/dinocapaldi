<?php
/**
 * @var array $backtrace
 * @var $error
 * @var $file
 * @var $line
 * @var $errno
 */
?>
<table class="contentTable backtrace">
    <thead>
    <tr>
        <td colspan="4">
            <?= htmlspecialchars($error) ?><br />
            File: <?= htmlspecialchars($file) ?><br />
            Line: <?= htmlspecialchars($line) ?><br />
            Errno: <?= htmlspecialchars($errno) ?>
        </td>
    </tr>
    <tr>
        <th>File</th>
        <th>Class</th>
        <th>Function</th>
        <th>Line</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($backtrace as $stackInfo) { ?>
        <tr>
            <td><?= htmlspecialchars(empty($stackInfo['file']) ? 'N/A' : $stackInfo['file']) ?></td>
            <td><?= htmlspecialchars(empty($stackInfo['class']) ? 'N/A' : $stackInfo['class']) ?></td>
            <td><?= htmlspecialchars(empty($stackInfo['function']) ? 'N/A' : $stackInfo['function']) ?></td>
            <td><?= htmlspecialchars(empty($stackInfo['line']) ? 'N/A' : $stackInfo['line']) ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>