<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <?= $this->js('/js/jquery.min.js') ?>
    <?= $this->js('/js/jquery.tools.min.js') ?>
    <?= $this->js('/js/jquery.mousewheel.js') ?>
    <?= $this->js('/js/textbox.js') ?>
    <?= $this->js('/js/textarea.js') ?>
    <?= $this->js('/js/selectbox.js') ?>
    <?= $this->js('/js/passwordbox.js') ?>
    <?= $this->js('/js/swfobject.js') ?>
    <?= $this->js('/js/jwplayer.js') ?>
    <?= $this->js('/js/form.js') ?>
    <?= $this->js('/js/fileuploader.js') ?>
    <?= $this->js('/js/logic.js') ?>
    <?= $this->js('/js/date.js') ?>
    <?= $this->css('/css/jquery.jscrollpane.css') ?>
    <?= $this->css('/css/layout.css') ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<?= $content_for_layout ?>
</body>
</html>